//
//  DetailDogDataView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/23/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import MapKit
import CoreLocation

class DetailDogDataView: UIViewController, MKMapViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UITabBarControllerDelegate {
    var dogObjectDetails = [PFObject]()
    var catChosen = [PFObject]()
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    var phone:String!
    var galleryArray = [String]()
    var websiteString:String!
    var hours:String!
    var services:String!
    var address:String!
    var image:PFFile!
    var categoryRating:Double!
    var currObjId:String!
    var userLocation: CLLocation?
    var location:CLLocation!
    var counter:Int!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var phoneLabelButton: UIButton!
    @IBOutlet var collectionView: UICollectionView!    
    @IBOutlet var newRating: CosmosView!
    @IBOutlet var servicesLabel: UITextView!

    @IBOutlet var hoursLabel: UITextView!
    @IBOutlet var addressLabel: UILabel!

    @IBOutlet var websiteLabel: UIButton!
    @IBOutlet var callDesignButton: UIButton!
    @IBOutlet var directionsDesignButton: UIButton!
    @IBOutlet var reviewsLabel: UILabel!
    
    var websiteLabelButton:String!
    var name:String!
    var currLat:String!
    var currLng:String!
    @IBOutlet var currCompanyRating: CosmosView!

    @IBOutlet var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(name)
        
        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        currCompanyRating.settings.updateOnTouch = false
        
        navBar()

        buttonDesigns()
        
        tapGestures()
        
        
        galleryArray = ["chop1", "babychop", "bigchop"]
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
       queryDogObjects()
    }
    

    @IBAction func backButtonTapped(_ sender: Any) {
        
//        let controller = self.storyboard!.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
//        let controllerNav = UINavigationController(rootViewController: controller)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        controller.catChosen = self.catChosen
//        //controller.counter = self.counter
//        appDelegate.window?.rootViewController = controllerNav
        
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
//        vc.selectedIndex = 1
//        self.present(vc, animated: true, completion: nil)
        let keyUser = UserDefaults.standard
        keyUser.removeObject(forKey: "Key")
        keyUser.synchronize()
        
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                   vc.selectedIndex = 1

        for category in catChosen{
            print(category)
            let catName = category.object(forKey: "category_name")
            let users = UserDefaults.standard
            users.setValue(catName, forKey: "UserKey")
        }
        
                self.present(vc, animated: true, completion: nil)
    }

   
    @IBAction func callButtonTapped(_ sender: Any) {
        
        let url: NSURL = URL(string: self.phone)! as NSURL
        
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryImgCell
        let image = UIImage(named: galleryArray[indexPath.row])
        if let image = image{
            cell.galleryImg.image = image
        } else {
            cell.galleryImg.image = UIImage(named: "defaultImage")
        }
        
        return cell
    }
    
    
    
    func handleTap(_sender: UITapGestureRecognizer){
        
        let currentUser = PFUser.current()
        if currentUser != nil{
            
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "ReviewsAndRatingsView") as! ReviewsAndRatingsView
            let controllerNav = UINavigationController(rootViewController: controller)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            controller.ratingChosen = newRating.rating
            controller.companyName = name
            controller.catChosen = self.catChosen
            //controller.counter = self.counter
            controller.currObjId = currObjId
            
            controller.dogDetailObjects = self.dogObjectDetails
            appDelegate.window?.rootViewController = controllerNav
            
        } else {
           
            //request users signup
            let userSignUpAlert = UIAlertController(title: "We want to hear from you", message: "Signup or Sign in", preferredStyle: UIAlertControllerStyle.alert)
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                //self.newRating.rating = 0
                //take user to login view
                
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                let controllerNav = UINavigationController(rootViewController: controller)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                controller.rating = self.newRating.rating
                controller.companyName = self.name
                controller.catChosen = self.catChosen
                //controller.counter = self.counter
                controller.currObjId = self.currObjId
                
                controller.dogDetailObjects = self.dogObjectDetails
                appDelegate.window?.rootViewController = controllerNav
                
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { (action) in
                self.newRating.rating = 0
                
                
            })

            userSignUpAlert.addAction(defaultAction)
            userSignUpAlert.addAction(cancelAction)
            present(userSignUpAlert, animated: true, completion: nil)
        }
        
        
    }
    
    func handleRatingTap(_sender: UITapGestureRecognizer){
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "UserPostedReviews") as! UserPostedReviews
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.companyName = name
        controller.catChosen = self.catChosen
        controller.dogObjectDetails = self.dogObjectDetails
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func handleImgViewTap(_sender: UITapGestureRecognizer){
        
        if let url = URL(string: websiteLabelButton) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    func handleReviewLabelTap(_sender: UITapGestureRecognizer){
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "UserPostedReviews") as! UserPostedReviews
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.companyName = name
        controller.catChosen = self.catChosen
        controller.dogObjectDetails = self.dogObjectDetails
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }

    
    
    @IBAction func websiteButtonTapped(_ sender: Any) {
        
        if let url = URL(string: websiteLabelButton) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    @IBAction func directionsButtonTapped(_ sender: Any) {

        goToMap()
    }
    
    @IBAction func promoButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "PromotionsView") as! PromotionsView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.ratingChosen = newRating.rating
        controller.companyName = name
        controller.catChosen = self.catChosen
        //controller.counter = self.counter
        controller.currObjId = currObjId
        
        controller.dogDetailObjects = self.dogObjectDetails
        appDelegate.window?.rootViewController = controllerNav
    }
    
    
    
    func goToMap(){
        
        let location = CLLocation(latitude: Double(currLat)!, longitude: Double(currLng)!)
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            print(placeMark.addressDictionary!)
            
            if let street = placeMark.addressDictionary!["Street"] as? NSString{
                
                
                
                let latitude:CLLocationDegrees =  Double(self.currLat)!
                let longitude:CLLocationDegrees =  Double(self.currLng)!
                
                let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
                
                let placemark : MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:nil)
                
                let mapItem:MKMapItem = MKMapItem(placemark: placemark)
                
                mapItem.name = street as String
                
                let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey as NSCopying)
                
                let currentLocationMapItem:MKMapItem = MKMapItem.forCurrentLocation()
                
                MKMapItem.openMaps(with: [currentLocationMapItem, mapItem], launchOptions: launchOptions as? [String : Any])

                
            }
        }

        
        
    }
    
    func buttonDesigns(){
        callDesignButton.layer.cornerRadius = 5
        callDesignButton.layer.borderWidth = 1
        
        directionsDesignButton.layer.cornerRadius = 5
        directionsDesignButton.layer.borderWidth = 1
    }
    
    func tapGestures(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_sender:)))
        let imgTap = UITapGestureRecognizer(target: self, action: #selector(handleImgViewTap(_sender:)))
        let ratingTap = UITapGestureRecognizer(target: self, action: #selector(handleRatingTap(_sender:)))
        let reviewLabelTap = UITapGestureRecognizer(target: self, action: #selector(handleReviewLabelTap(_sender:)))
        
        reviewsLabel.addGestureRecognizer(reviewLabelTap)
        reviewsLabel.isUserInteractionEnabled = true
        view.addSubview(reviewsLabel)
        
        imgView.addGestureRecognizer(imgTap)
        imgView.isUserInteractionEnabled = true
        view.addSubview(imgView)
        
        currCompanyRating.addGestureRecognizer(ratingTap)
        currCompanyRating.isUserInteractionEnabled = true
        view.addSubview(currCompanyRating)
        
        newRating.addGestureRecognizer(tap)
        newRating.isUserInteractionEnabled = true
        view.addSubview(newRating)

    }

    func queryDogObjects(){
        for obj in dogObjectDetails{
            
            currLat = obj["lat"]as! String
            currLng = obj["lng"]as! String
            let currName:String! = obj["name"]as! String
            if let currName = currName{
                name = currName
            }
            let currServices:String! = obj["services"]as! String
            if let currServices = currServices{
                servicesLabel.text = currServices
            }
            let currHours:String! = obj.object(forKey: "hours")as! String
            if currHours != nil{
                hoursLabel.text = currHours
            }

            let currPhoneNum:String! = obj.object(forKey: "phoneNum")as! String
            if currPhoneNum != nil{
                let num = "Call " + currPhoneNum + ""
                phoneLabelButton.setTitle(num, for: .normal)
                
                let result = String(currPhoneNum.characters.filter { "01234567890.".characters.contains($0)})
                print(result)
                self.phone = "TEL://\(result)"

            }
            let currWebsite:String! = obj["website"]as! String
            if currWebsite != nil{
                websiteLabelButton = currWebsite
                //websiteLabel.setTitle(currWebsite, for: .normal)
            }
            let currAddress:String! = obj["address"]as! String
            if currAddress != nil{
                addressLabel.text = currAddress
            }
            let currImg:PFFile! = obj["images"]as! PFFile
            if currImg != nil{
                currObjId = obj.objectId
                
                let imgFileURL:NSURL = NSURL(string: currImg.url!)!
                let imageData:NSData = (NSData(contentsOf: imgFileURL as URL))!
                imgView.image = UIImage(data: imageData as Data)!
            } else {
                imgView.image = UIImage(named: "defaultImage")
            }
            let currRating:String! = obj["rating"]as! String
            if currRating != nil{
                currCompanyRating.rating = Double(currRating)!
            }
           
        }

    }
}
