//
//  UserPostedReviews.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/31/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
class UserPostedReviews: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    var companyName:String!
    var userReviewObj = [PFObject]()
    var total:Int = 0
    var catChosen = [PFObject]()
    var dogObjectDetails = [PFObject]()
    var userReview:String!
    var userRating:String!
    var counter:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        reviewNavBar()
       
        let query = PFQuery(className: "Reviews")
        query.whereKey("name", contains: companyName)
        query.whereKey("name", equalTo: companyName)
        query.findObjectsInBackground { (success, error) in
            
            if success != nil{
                self.userReviewObj = success!
                if self.userReviewObj == []{
                    print("no data")
                    let alert = UIAlertController(title: "Alert", message: "No Reviews Posted...Be the first", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
                        let controllerNav = UINavigationController(rootViewController: controller)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        controller.catChosen = self.catChosen
                        controller.dogObjectDetails = self.dogObjectDetails
                        controller.counter = self.counter
                        appDelegate.window?.rootViewController = controllerNav
                    })
                    
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.tableView.reloadData()
                }
               self.tableView.reloadData()
            } else {
                print(error?.localizedDescription as Any)
            }
        }

       
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userReviewObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UserPostedReviewsCell
        cell.selectionStyle = .none
        let listData:PFObject = self.userReviewObj[indexPath.row]
        let rating = listData.object(forKey: "rating")as? String
        let dblRating = Double(rating!)
        let reviewerImg = listData.object(forKey: "reviewerImg")as? PFFile
        let reviwerUsername = listData.object(forKey: "reviewer")as? String
//        cell.usersReviewLabel.text = listData.object(forKey: "review")as? String
//        cell.usersReviewLabel.numberOfLines = 1
//        cell.datePost.text = listData.object(forKey: "date")as? String
//        cell.userRating.rating = dblRating!
        //cell.userRating.text = listData.object(forKey: "rating")as? String
//        cell.username.text = reviwerUsername
        if let reviewerImg = reviewerImg{
            cell.userImg.loadImageUsingCacheWithUrlString(urlString: reviewerImg)
            cell.usersReviewLabel.text = listData.object(forKey: "review")as? String
            cell.usersReviewLabel.numberOfLines = 1
            cell.datePost.text = listData.object(forKey: "date")as? String
            cell.userRating.rating = dblRating!
            cell.username.text = reviwerUsername
        } else {
           
            cell.userImg.image = UIImage(named: "defaultImage")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let listData = self.userReviewObj[indexPath.row]
        userReview = listData.object(forKey: "review")as? String
        userRating = listData.object(forKey: "rating")as? String
        if let cell = tableView.cellForRow(at: indexPath) as? UserPostedReviewsCell {
            let label = cell.usersReviewLabel
            tableView.beginUpdates()
            label?.numberOfLines = label?.numberOfLines == 0 ? 1 : 0
            tableView.endUpdates()
        }
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailUserReview") as! DetailUserReview
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.userReviewObj = self.userReviewObj
        controller.dogObjectDetails = self.dogObjectDetails
        controller.catChosen = self.catChosen
        controller.companyName = self.companyName
        controller.latestReview = userReview
        controller.latestRating = userRating
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.catChosen = self.catChosen
        controller.dogObjectDetails = self.dogObjectDetails
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }
  
    
}

