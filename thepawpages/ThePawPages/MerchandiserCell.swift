//
//  MerchandiserCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 5/9/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class MerchandiserCell: UITableViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var companyNameLabel: UILabel!

    @IBOutlet var phoneNumLabel: UILabel!
    @IBOutlet var servicesLabel: UILabel!


}
