//
//  MerchandiserView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 5/9/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Parse

class MerchandiserView: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet var tableView: UITableView!
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var location:CLLocation! = nil
    var distanceInMiles:Double!
    var latDbl:Double!
    var lngDbl:Double!
    var latestLocation:CLLocation!
    var doubleStr:String!
    var merchandisers = [PFObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }
        
        
        navBar()
        tableView.delegate = self
        tableView.dataSource = self
        merchandiserQuery()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchandisers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MerchandiserCell
        let listData:PFObject = self.merchandisers[indexPath.row]
        cell.companyNameLabel.text = listData.object(forKey: "name") as? String
        cell.phoneNumLabel.text = listData.object(forKey: "phoneNum") as? String
        cell.servicesLabel.text = listData.object(forKey: "services") as? String
        
        let image = listData.object(forKey: "image") as! PFFile
        cell.imgView.loadImageUsingCacheWithUrlString(urlString: image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let listData = self.merchandisers[indexPath.row]
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "MerchandiserDetailView") as! MerchandiserDetailView
        
        
        let controllerNav = UINavigationController(rootViewController: controller)
        //let newController = UITabBarController(nibName: "ViewController", bundle: .main)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        controller.listData = [listData]
        
        appDelegate.window?.rootViewController = controllerNav
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        
//        
//        
//        if startLocation == nil{
//            //startLocation = latestLocation as CLLocation
//            latestLocation = locations.last
//            startLocation = latestLocation
//            
//            
//            let theLat = latestLocation.coordinate.latitude
//            let theLng = latestLocation.coordinate.longitude
//            
//            let location = CLLocation(latitude: Double(theLat), longitude: Double(theLng))
//            let geoCoder = CLGeocoder()
//            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
//                
//                var placeMark: CLPlacemark!
//                placeMark = placemarks?[0]
//                
//                print(placeMark.addressDictionary!)
//                let city = placeMark.addressDictionary!["City"] as? NSString
//                let state = placeMark.addressDictionary!["State"] as? NSString
//                
//             
//                
//                self.locationManager.stopUpdatingLocation()
//                
//            }
//            locationManager.stopUpdatingLocation()
//            
//        }
//        self.locationManager.stopUpdatingLocation()
//    }
//    
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        
//        locationRequestDenied()
//        
//    }

    @IBAction func cameraButtonTapped(_ sender: Any) {
        
    }
    
    func merchandiserQuery(){
        let query:PFQuery = PFQuery(className: "Merchandiser")
        query.findObjectsInBackground{ (objects, error) in
            if objects != nil {
                self.merchandisers = objects!
                if self.merchandisers == []{
                   //no merchandiser data
                } else {
                    self.tableView.reloadData()
                    //data exist
                }
            }
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        
        let controllerNav = UINavigationController(rootViewController: controller)
        //let newController = UITabBarController(nibName: "ViewController", bundle: .main)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
    }
    
}
