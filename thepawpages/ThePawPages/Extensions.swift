//
//  Extensions.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/7/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
let imageCache = NSCache<AnyObject, AnyObject>()
let textCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: PFFile){
        
        self.image = nil
        
        
        //check cache for image first
        
        if let cachedImage = imageCache.object(forKey: urlString ) {
            self.image = cachedImage as? UIImage
            return
        }
        
        //otherwise  create new image
        //let url = NSURL(string: urlString)
        let url:NSURL = NSURL(string: (urlString.url!))!
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = URLRequest(url: url as URL)
        
        session.dataTask(with: request) {
            (data, response, error) in
            if error != nil {
                print(error!)
            } else {
                DispatchQueue.main.async {
                    
                    if let downloadedImage = UIImage(data: data!){
                        imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                        
                        self.image = downloadedImage
                    }
                    
                }
                
            }
            }.resume()
        
    }
}


extension UILabel{
    
    func loadLabelUsingCacheWithUrlString(urlString: String!){
        
        self.text = nil
        
        if textCache.object(forKey: urlString as String as AnyObject) != nil{
            self.text = textCache as? AnyObject as? String
            return
        }
    }
    
}
extension CosmosView{
    
}

