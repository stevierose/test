//
//  DOgAdoptonView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/20/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class DogAdoptonView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        var url = NSURL(string: "http://www.arrfnc.com/animals/detail?AnimalID=10448050")
        
        if url != nil {
            let task = NSURLSession.sharedSession().dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
                print(data)
                
                if error == nil {
                    
                    var urlContent = NSString(data: data, encoding: NSASCIIStringEncoding) as NSString!
                    
                    print(urlContent)
                }
            })
            task.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
