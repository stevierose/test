//
//  AdDetailView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/13/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
class AdDetailView: UIViewController {

    var dogObjects = [PFObject]()
    var catChosen = [PFObject]()
    var counter:Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        navBar()
        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.dogObjects = self.dogObjects
        controller.catChosen = self.catChosen
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }

}
