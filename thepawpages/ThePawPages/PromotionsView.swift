//
//  PromotionsView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/24/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import  Alamofire
import AlamofireImage


class PromotionsView: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var actIndicator: UIActivityIndicatorView!
    @IBOutlet var actIndicatorView: UIView!
    let cellSpacingHeight: CGFloat = 10
    var promoObjects = [PFObject]()
    @IBOutlet var imageDog: UIImageView!
    @IBOutlet var tableView: UITableView!
    var ratingChosen:Double!
    var catChosen = [PFObject]()
    var companyName:String!
    var currObjId:String!
    var dogDetailObjects = [PFObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        navBar()
        actIndicator.startAnimating()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        promoQuery()
       
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return promoObjects.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PromotionsViewCell
        let listData:PFObject = promoObjects[indexPath.section]
        let url = listData.object(forKey: "image") as! String
        
        Alamofire.request(url).responseImage(completionHandler: { (response) in
            if let image = response.result.value{
                self.actIndicator.stopAnimating()
                self.actIndicator.isHidden = true
                self.actIndicatorView.isHidden = true
                
               cell.promoImg.image = image
            }
        })
        
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true

        return cell
        
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        //back button should toggle depending on how user got there
        if currObjId != nil{
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
            let controllerNav = UINavigationController(rootViewController: controller)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print(self.ratingChosen)
            controller.dogObjectDetails = self.dogDetailObjects
            controller.name = self.companyName
            controller.catChosen = self.catChosen
            controller.currObjId = self.currObjId
            appDelegate.window?.rootViewController = controllerNav
        } else {
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                                    vc.selectedIndex = 0
                                    self.present(vc, animated: true, completion: nil)
        }
       
    }

    func promoQuery(){
        
        let userDefaults = UserDefaults.standard
        let name = userDefaults.string(forKey: "Key")
        
        let query = PFQuery(className:"Promotions")
       
        if name != nil{
             query.whereKey("name", contains: name)
            //clear defaults
            userDefaults.removeObject(forKey: "Key")
            userDefaults.synchronize()
            
        }
       
        query.findObjectsInBackground { (objects, error) in
            
            if error != nil {
                print(error?.localizedDescription as Any)
            } else {
            
                self.actIndicator.stopAnimating()
                self.actIndicator.isHidden = true
                self.actIndicatorView.isHidden = true
                
                self.promoObjects = objects!
                print(self.promoObjects)
                if self.promoObjects == [] {
                    
                    if name != nil{
                        let alertController = UIAlertController(title: "Check back soon!", message: "No current promotions", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                            
                            //                        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            //                        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "detailTabBarController") as! UITabBarController
                            //                        vc.selectedIndex = 0
                            
                            
                            let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
                            let controllerNav = UINavigationController(rootViewController: controller)
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            print(self.ratingChosen)
                            controller.dogObjectDetails = self.dogDetailObjects
                            controller.name = self.companyName
                            controller.catChosen = self.catChosen
                            controller.currObjId = self.currObjId
                            appDelegate.window?.rootViewController = controllerNav
                            
                            
                            //                        if let vcs = vc.viewControllers,
                            //                            let nc = vcs.first as? UINavigationController,
                            //                            let pendingOverVC = nc.topViewController as? DetailDogDataView {
                            //                            pendingOverVC.dogObjectDetails = [listData]
                            //                            pendingOverVC.catChosen = self.catChosen
                            //
                            //                        }
                            //                                                self.present(vc, animated: true, completion: nil)
                            
                            
                        })
                        
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                        return
                    }
                    let alertController = UIAlertController(title: "Check back soon!", message: "No current promotions", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "detailTabBarController") as! UITabBarController
                        vc.selectedIndex = 0
                        
                        
//                        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
//                        let controllerNav = UINavigationController(rootViewController: controller)
//                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                        print(self.ratingChosen)
//                        controller.dogObjectDetails = self.dogDetailObjects
//                        controller.name = self.companyName
//                        controller.catChosen = self.catChosen
//                        controller.currObjId = self.currObjId
//                        appDelegate.window?.rootViewController = controllerNav

                        
                        //                        if let vcs = vc.viewControllers,
                        //                            let nc = vcs.first as? UINavigationController,
                        //                            let pendingOverVC = nc.topViewController as? DetailDogDataView {
                        //                            pendingOverVC.dogObjectDetails = [listData]
                        //                            pendingOverVC.catChosen = self.catChosen
                        //
                        //                        }
//                                                self.present(vc, animated: true, completion: nil)
                        
                        
                    })
                    
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                   
                    
                } else {
                    print(self.promoObjects)
                    self.tableView.reloadData()
                }
               
            }
        }
    }
}
