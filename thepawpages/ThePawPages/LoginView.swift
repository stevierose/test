//
//  LoginView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/7/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4
import FBSDKCoreKit

class LoginView: UIViewController, UITextFieldDelegate {

    var name:String!
    var rating:Double!
    var companyName:String!
    var catChosen = [PFObject]()
    var currObjId:String!
    var dogDetailObjects = [PFObject]()
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var usernameTextfield: UITextField!
    @IBOutlet var facebookButton: UIButton!
    @IBOutlet var guestUserButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        activityIndicator.isHidden = true

        checkIfExistingUser()
        
        buttonDesigns()
        
        passwordTextfield.delegate = self
        
        hideKeyboardWhenTappedAround()
        
        navBar()
        
 

    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        guard let username = usernameTextfield.text, let password = passwordTextfield.text else{
            return
        }
        PFUser.logInWithUsername(inBackground: username, password: password) { (user:PFUser?, error) in
            if error != nil{
                
                let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                print(error?.localizedDescription as Any)
            } else {
                //if user has rating send to review page
                if self.rating == .none{
                    //send user to Landing Page View Controller
//                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
//                    
//                    self.present(vc, animated: true, completion: nil)
                    
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    let controllerNav = UINavigationController(rootViewController: controller)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    appDelegate.window?.rootViewController = controllerNav
                } else {
                    //send user to reivew Controller
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ReviewsAndRatingsView") as! ReviewsAndRatingsView
                    let controllerNav = UINavigationController(rootViewController: controller)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    controller.ratingChosen = self.rating
                    
                    controller.companyName = self.companyName
                    controller.catChosen = self.catChosen
                    //controller.counter = self.counter
                    controller.currObjId = self.currObjId
                    
                    controller.dogDetailObjects = self.dogDetailObjects
                    appDelegate.window?.rootViewController = controllerNav
                }
                
                
//                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//                let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//                self.present(vc, animated: true, completion: nil)

            }
        }
    }

    @IBAction func guestUserButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
        
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
       
        PFFacebookUtils.logInInBackground(withReadPermissions: ["public_profile", "email"]) { (user:PFUser?, error) in
            
            
            if let user = user {
                if user.isNew {
                    
                    self.initialUserViewSaved()
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                    
                    print("User signed up and logged in through Facebook!")
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    let controllerNav = UINavigationController(rootViewController: controller)
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                    appDelegate.window?.rootViewController = controllerNav
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)

                } else {
                    
                    self.initialUserViewSaved()
                    
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                    
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                                    let controllerNav = UINavigationController(rootViewController: controller)
//                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                                    appDelegate.window?.rootViewController = controllerNav
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)
                    print("User logged in through Facebook!")
                }
            } else {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
//                let alertController = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
//                
//                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
//                
//                                })
//                                alertController.addAction(defaultAction)
//                                self.present(alertController, animated: true, completion: nil)
                print("Uh oh. The user cancelled the Facebook login.")
            }
            
            
//            if error != nil{
//                print(error?.localizedDescription as Any)
//                
//                let alertController = UIAlertController(title: "Alert", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
//                
//                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
//                    
//                })
//                alertController.addAction(defaultAction)
//                self.present(alertController, animated: true, completion: nil)
//            } else {
//             
//                
//                let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                let controllerNav = UINavigationController(rootViewController: controller)
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                
//                appDelegate.window?.rootViewController = controllerNav
//            }
        }
    }
    
    
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordView") as! ForgotPasswordView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }
    
    @IBAction func registrationButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "SignUpView") as! SignUpView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func loginInUser(username:String, password:String){
        PFUser.logInWithUsername(inBackground: username, password: password) { (success, error) in
            if success != nil{
                
                self.initialUserViewSaved()
                
                //take user to landing page
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if self.rating == .none{
                    //send user to Landing Page View Controller
//                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    let controllerNav = UINavigationController(rootViewController: controller)
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    
//                    appDelegate.window?.rootViewController = controllerNav
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                    
                    self.present(vc, animated: true, completion: nil)
                    
                } else {
                    //send user to reivew Controller
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ReviewsAndRatingsView") as! ReviewsAndRatingsView
                    let controllerNav = UINavigationController(rootViewController: controller)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    controller.ratingChosen = self.rating
                    
                    controller.companyName = self.companyName
                    controller.catChosen = self.catChosen
                    //controller.counter = self.counter
                    controller.currObjId = self.currObjId
                    
                    controller.dogDetailObjects = self.dogDetailObjects
                    appDelegate.window?.rootViewController = controllerNav
                }

                
//                let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                let controllerNav = UINavigationController(rootViewController: controller)
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                
//                appDelegate.window?.rootViewController = controllerNav
                
//                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//                let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//                self.present(vc, animated: true, completion: nil)
                
            } else {
                
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription as Any as? String, preferredStyle: UIAlertControllerStyle.alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                print(error?.localizedDescription as Any)
            }
        }
    
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        passwordTextfield.resignFirstResponder()
        loginInUser(username: usernameTextfield.text!, password: passwordTextfield.text!)
        return true
    }
    

    
    func buttonDesigns(){
        
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        
        facebookButton.layer.cornerRadius = 5
        facebookButton.layer.borderWidth = 1

    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "FirstTimeLoginView") as! FirstTimeLoginView
                self.present(vc, animated: true, completion: nil)
        
    }
    func loadLandingView(){
    
    }
}
