//
//  DogDetailCollectionCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/23/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class DogDetailCollectionCell: UICollectionViewCell {
    @IBOutlet var pupImages: UIImageView!
    
}
