//
//  DOgAdoptonView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 4/20/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import  Alamofire
import AlamofireImage
import Parse
import CoreLocation
import MapKit

class DogAdoptonView: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet var actIndicator: UIActivityIndicatorView!
    @IBOutlet var actView: UIView!
    var availableStatus:String! = nil
    var allDogs = [AdoptablePups]()
    var myCat:PFObject!
    var url:String!
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var location:CLLocation! = nil
    var distanceInMiles:Double!
    var latDbl:Double!
    var lngDbl:Double!
    var latestLocation:CLLocation!
    var doubleStr:String!
//    var city:String!
//    var state:String!


    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        
        navBar()

        actView.isHidden = false
        actIndicator.isHidden = false
        actIndicator.startAnimating()
        
        tableView.delegate = self
        tableView.dataSource = self
        

        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allDogs.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DogAdoptionCell
        cell.nameLabel.isHidden = true
        cell.breedLabel.isHidden = true
        cell.ageLabel.isHidden = true
        cell.sexLabel.isHidden = true
        cell.pupImg.isHidden = true
        
        
        let listData = allDogs[indexPath.row]
        print(listData.photos)
        //cell.pupImg.image = UIImage(named: listData.photos)
        Alamofire.request(listData.photos).responseImage(completionHandler: { (response) in
            if let image = response.result.value{
                print(image)
                cell.nameLabel.text = listData.name
                cell.breedLabel.text = listData.breed
                cell.ageLabel.text = listData.age
                cell.sexLabel.text = listData.sex
                cell.pupImg.image = image
                
                cell.nameLabel.isHidden = false
                cell.breedLabel.isHidden = false
                cell.ageLabel.isHidden = false
                cell.sexLabel.isHidden = false
                cell.pupImg.isHidden = false
                
                //self.tableView.reloadData()
                                                       
            }
            })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let listData = allDogs[indexPath.row]
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DogAdoptionDetailView") as! DogAdoptionDetailView
        
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        controller.listData = [listData]
        
        appDelegate.window?.rootViewController = controllerNav

    }

    func loadDogData(city:String, state:String){
    
        Alamofire.request("http://api.petfinder.com/pet.find?key=f7f57dc82292a612a98e02338f78680f&animal=dog&location=" +  city + "," + state + "&output=full&format=json").responseJSON{(response) in
           
            
            if let jsonDict = response.result.value as? [String:Any]{
               
                let petFinder = jsonDict["petfinder"] as? [String:Any]
                let pets = petFinder?["pets"] as? [String:Any]
                let pet = pets?["pet"] as? [[String:Any]]
                for availableDogs in pet!{
                    let adoptablePups = AdoptablePups()
                    
    
                    
                    if let available = availableDogs["status"] as? [String:Any]{
                        for (key,value) in available{
                            if "$t" == key{
                                //dogs available
                                self.availableStatus = value as! String
                            } else {
                                //dogs not available
                                
                            }
                        }
                    }
                    
                    if self.availableStatus != nil{
                        
                        if let size = availableDogs["size"] as? [String:Any]{
                            for (key,value) in size{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.size = value as! String
                                }
                            }
                        }
                        let media = availableDogs["media"] as? [String:Any]
                        let photos = media?["photos"] as? [String:Any]
                        if let photo = photos?["photo"] as? [[String:Any]]{
                            for allPhotos in photo{
                                for (key,value) in allPhotos{
                                   
                                    if "$t" == key{
                                        //append photo array
                                       
                                        self.url = value as! String
 
                                    }
                                }
                                print(self.url)
                                
                            }
                                adoptablePups.photos = self.url
                          
                        }
                        
                        let breeds = availableDogs["breeds"] as? [String:Any]
                        if let breed = breeds?["breed"] as? [String:Any]{
                            
                            for (key,value) in breed{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.breed = value as! String
                                }
                            }
                        }
                        if let mixedBreed = breeds?["breed"] as? [[String:Any]]{
                           var mixedBreeds = String(describing: mixedBreed)
                            mixedBreeds =  mixedBreeds.replacingOccurrences(of: "\"\\$t\"\\:", with: "",options: .regularExpression)
                            var anotherMixedBreed = mixedBreeds
                            anotherMixedBreed = anotherMixedBreed.replacingOccurrences(of: "[", with: "")
                            print(anotherMixedBreed)
                            var finalMixedBreed = anotherMixedBreed
                            finalMixedBreed = finalMixedBreed.replacingOccurrences(of: "]", with: "")

                            
                           adoptablePups.breed = finalMixedBreed
                        }
                        if let contact = availableDogs["contact"] as? [String:Any]{
                            let email = contact["email"] as? [String:Any]
                            for (key,value) in email!{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.email = value as! String
                                }
                            }
                            
                        }
                        
                        if let options = availableDogs["options"] as? [String:Any]{
                            if let option = options["option"] as? [[String:Any]]{
                                for petOptions in option{
                                    let shots = petOptions["$t"] as? String
                                    if "hasShots" == shots{
                                        print(shots!)
                                        adoptablePups.hasShots = shots!
                                    }
                                    
                                    if "housetrained" == shots{
                                        print(shots!)
                                        adoptablePups.houseTrained = shots!
                                    }
                                }
                            }
                            
                            
                        }
                        
                        if let age = availableDogs["age"] as? [String:Any]{
                            for (key,value) in age{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.age = value as! String
                                }
                            }
                        }
                        
                        if let name = availableDogs["name"] as? [String:Any]{
                            for (key,value) in name{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.name = value as! String
                                }
                            }
                        }
                        
                        if let sex = availableDogs["sex"] as? [String:Any]{
                            for (key,value) in sex{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.sex = value as! String
                                }
                            }
                        }
                        
                        if let description = availableDogs["description"] as? [String:Any]{
                            for (key,value) in description{
                                if "$t" == key{
                                    print(value)
                                    adoptablePups.description = value as! String
                                }
                            }
                        }

                        self.allDogs.append(adoptablePups)
                      
                        DispatchQueue.main.async {
                            
                            self.tableView.reloadData()
                            
                            self.actIndicator.stopAnimating()
                            self.actView.isHidden = true
                            self.actIndicator.isHidden = true
                        }
                        

                    } else {
                        //dog not available
                    }
                    
                }
            }
            
        }
    }
   
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
        
        self.present(vc, animated: true, completion: nil)
        
//        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//       
//        
//        let controllerNav = UINavigationController(rootViewController: controller)
//        //let newController = UITabBarController(nibName: "ViewController", bundle: .main)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        controller.myCat = myCat
//        
//        appDelegate.window?.rootViewController = controllerNav
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        
        if startLocation == nil{
            //startLocation = latestLocation as CLLocation
            latestLocation = locations.last
            startLocation = latestLocation
            
            
            let theLat = latestLocation.coordinate.latitude
            let theLng = latestLocation.coordinate.longitude
            
            let location = CLLocation(latitude: Double(theLat), longitude: Double(theLng))
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                
                print(placeMark.addressDictionary!)
                let city = placeMark.addressDictionary!["City"] as? NSString
                let state = placeMark.addressDictionary!["State"] as? NSString
                
                self.loadDogData(city: city! as String, state: state! as String)
                
                self.locationManager.stopUpdatingLocation()
 
            }
            locationManager.stopUpdatingLocation()
            
        }
        self.locationManager.stopUpdatingLocation()
    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       
        locationRequestDenied()

    }

}
