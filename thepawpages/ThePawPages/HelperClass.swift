//
//  File.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/30/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import Foundation
import UIKit
import Parse
import FBSDKCoreKit
import ParseFacebookUtilsV4
import CoreData
import CoreLocation
import ReachabilitySwift

extension UIViewController{
    
   
    func faceBookParams(){
        let requestParameters = ["fields": "id, email, first_name, last_name"]
        
        let userDetails = FBSDKGraphRequest(graphPath: "me", parameters: requestParameters)
        userDetails?.start(completionHandler: { (connection, result, error) in
            
            if error != nil{
                print(error?.localizedDescription as Any)
                return
            }
            
            if let result = result as? [String:AnyObject] {
                let username = result["first_name"]as? String
                
                let email = result["email"]as? String
                print(email!)
                
                let userId = result["id"]as? String
                print(userId!)
                
                let currentUser:PFUser = PFUser.current()!
                
                if username != nil{
                    currentUser.setObject(username!, forKey: "username")
                }
                
                if email != nil{
                    currentUser.setObject(email!, forKey: "email")
                    print(email!)
                }
                
                if userId != nil{
                    currentUser.setObject(userId!, forKey: "profile_id")
                }
                DispatchQueue.main.async {
                    
                    let userProfile = "https://graph.facebook.com/" + userId! + "/picture?type=large"
                    
                    let profilePictrueUrl = NSURL(string: userProfile)
                    
                    let profilePictureData = NSData(contentsOf: profilePictrueUrl as! URL)
                    print(profilePictureData!)
                    if profilePictureData != nil{
                        let profileFileObject = PFFile(data: profilePictureData! as Data)
                        currentUser.setObject(profileFileObject!, forKey: "profile_picture")
                        
                        currentUser.saveInBackground(block: { (success, error) in
                            if success{
                                print("all details are now updated")
                            } else {
                                print(error?.localizedDescription as Any)
                            }
                        })
                    }
                }
                
            }
            
        })
    }
    
    func checkIfExistingUser(){
        let currentUser = PFUser.current()
        if currentUser != nil {
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let controllerNav = UINavigationController(rootViewController: controller)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = controllerNav
        }
    }
    


    func navBar (){
        if let nav = navigationController?.navigationBar{
           
            nav.backgroundColor = UIColor(red: 0.23, green: 0.58, blue: 0.20, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.23, green: 0.58, blue: 0.20, alpha: 1.0)
            let logo = UIImage(named: "pawpagesnavlogo")
            let imageView = UIImageView(image: logo)
            nav.topItem?.titleView = imageView
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            
            
        }

    }
    
    func reviewNavBar (){
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.00, green: 0.52, blue: 0.76, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.00, green: 0.52, blue: 0.76, alpha: 1.0)
            
            nav.topItem?.title = "Reviews"
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            
            
        }
        
    }

    
    func alertPasswordError(){
        let alertController = UIAlertController(title: "Oops!", message: " Your password cannot contain special characters, must contain 1 uppercase character and be at least 6 characters long. Please try again", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }


    func alertEmailError(){
        let alertController = UIAlertController(title: "Oops!", message: "Email is invalid", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertUsernameError(){
        let alertController = UIAlertController(title: "Oops!", message: "Invalid username, cannot be longer than 15 characters", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }


    func alertImgNotAdded(){
        let alertController = UIAlertController(title: "Oops!", message: "Tap addd photo and say Cheese...for us", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func initialUserViewSaved(){
        var initialViewSkip: [NSManagedObject] = []
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "Login",
                                       in: managedContext)!
        
        let user = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        // 3
        user.setValue("userSkipped", forKeyPath: "skipped")
        
        // 4
        do {
            try managedContext.save()
            initialViewSkip.append(user)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

   

    func checkIfLoginSkipped(){
        var initialViewSkip: [NSManagedObject] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Login")
        do{
            initialViewSkip = try managedContext.fetch(fetchRequest)
            
            print(initialViewSkip.count)
            if initialViewSkip.count >= 1{
                //dont do anything
                print("count 0")
            } else {
                //send to initialView

//                let controller = self.storyboard!.instantiateViewController(withIdentifier: "FirstTimeLoginView") as! FirstTimeLoginView
//                let controllerNav = UINavigationController(rootViewController: controller)
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                
//                appDelegate.window?.rootViewController = controllerNav
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "FirstTimeLoginView") as! FirstTimeLoginView
                
                self.present(vc, animated: true, completion: nil)
                
                return

            }
            
        } catch let error as NSError{
            
            print(error)
            
        }
    }
    
    func locationRequestDenied(){
        let alertController = UIAlertController(title: "Location Accees Requested",
                                                message: "The location permission was not authorized. Please enable it in Settings to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            
            // THIS IS WHERE THE MAGIC HAPPENS!!!!
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings as URL)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func isReachable()->Bool{
        var isConnected = false
        let reachability = Reachability()
        if (reachability?.isReachableViaWiFi)!{
            print("reahable by wifi")
            isConnected = true
        } else if (!(reachability?.isReachable)!){
            print("not reachable")
            isConnected = false
        } else if (reachability?.isReachableViaWWAN)! {
            print("cellular")
            isConnected = true
        }
        
        return isConnected
    }
    
    func addWifi(){

        
        let alertController = UIAlertController(title: "Wifi Accees Needed",
                                                message: "Please enable it in Settings to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            
            let url = URL(string: "App-Prefs:root=WIFI") //for WIFI setting app
            let app = UIApplication.shared
            app.open(url!, options: [:], completionHandler: nil)
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)

    }
    
    func  alertNoUsernameAdded(){
        let alertController = UIAlertController(title: "Oops!", message: "Enter a cool username", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    func alertNoPasswordAdded(){
        let alertController = UIAlertController(title: "Oops!", message: "Don't forget your password...it's important", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    func alertNoEmailAddressAdded(){
        let alertController = UIAlertController(title: "Oops!", message: "Please give us your email address", preferredStyle: UIAlertControllerStyle.alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) -> Void in
            
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)

    }

}


