//
//  UserPostedReviewsCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/31/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class UserPostedReviewsCell: UITableViewCell {
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var datePost: UILabel!
    @IBOutlet var username: UILabel!
    @IBOutlet var usersReviewLabel: UILabel!
    @IBOutlet var userRating: CosmosView!

 
}
