//
//  CategoryController.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/19/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import MapKit
import Foundation
import Alamofire




extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

class CategoryController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UISearchBarDelegate{
    @IBOutlet var adImageViewHolder: UIView!
    @IBOutlet var adImage: UIImageView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var catChosen = [PFObject]()
    var dogObjects = [PFObject]()
    let regionRadius: CLLocationDistance = 1000
    var lng: String!
    var lat: String!
    var tempName = ""
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    var location:CLLocation! = nil
    var distanceInMiles:Double!
    var latDbl:Double!
    var lngDbl:Double!
    var latestLocation:CLLocation!
    var doubleStr:String!
    var total:Int = 0
    var name:String!
    var dblRating:Double!
    var cell:CategoryCell! = nil
    var searchResults = [String]()
    var searchActive : Bool = false
    var categoryName:String!
    var counter = 0
    var isImageAdded = true
    var myString:String!
    var companyName:String!
    let cellSpacingHeight: CGFloat = 10
    
    @IBOutlet var activityView: UIView!
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }
        
        
        //        adImageCounter()
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        
        navBar()
        
        activityView.isHidden = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.showsVerticalScrollIndicator = false
        
        //locationMgr Delegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        startLocation = nil
        
        let users = UserDefaults.standard
        let categoryChosen = users.string(forKey: "UserKey")
        if categoryChosen != nil{
            categoryName = categoryChosen
            search()
            categoryQuery()
            users.removeObject(forKey: "UserKey")
           
        } else if catChosen == []{
            queryAllCategories()
            search()
            users.removeObject(forKey: "UserKey")
            
        } else {
            
            for category in catChosen{
                categoryName = category.object(forKey: "category_name") as? String
                
                search()
                users.removeObject(forKey: "UserKey")
               
            }
            
            categoryQuery()
            
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dogObjects.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryCell
        
        
        activityView.isHidden = true
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        cell.companyRating.settings.updateOnTouch = false
        
        let listData:PFObject = self.dogObjects[indexPath.section]
        
        let name:String! = listData.object(forKey: "name")as! String
        if let name = name{
            self.companyName = name
            
            cell.comanyNameLbl.text = "\(name)"
            
        }
        
        
        let rating = listData.object(forKey: "rating")as? String
        if let userRating = rating{
            cell.companyRating.rating = Double(userRating)!
        } else {
            cell.companyRating.rating = 0
        }
        
        let address = listData.object(forKey: "address")as? String
        if let address = address{
            cell.companyAddress.text = address
        }else {
            cell.companyAddress.text = "no address listed"
        }
        
        
        let services = listData.object(forKey: "services")as? String
        if let services = services{
            cell.companyServices.text = services
        } else {
            cell.companyServices.text = "no services listed"
        }
        
        let images = listData.object(forKey: "images")as? PFFile
        if let images = images{
            cell.companyImg.loadImageUsingCacheWithUrlString(urlString: images)
        } else {
            cell.companyImg.image = UIImage(named: "defaultImage")
        }
        
        
        lng = listData.object(forKey: "lng")as! String
        lat = listData.object(forKey: "lat")as! String
        if let lat = lat, let lng = lng{
            self.latDbl = Double(lat)
            self.lngDbl = Double(lng)
            
            latestLocale()
        }
        
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        searchBar.resignFirstResponder()
        let listData:PFObject = self.dogObjects[indexPath.section]
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let name = listData.object(forKey: "name") as! String
        print(name)
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(name, forKey: "Key")
        controller.dogObjectDetails = [listData]
        controller.catChosen = self.catChosen
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
        
        //        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "detailTabBarController") as! UITabBarController
        //
        //        if let vcs = vc.viewControllers,
        //            let nc = vcs.first as? UINavigationController,
        //            let pendingOverVC = nc.topViewController as? DetailDogDataView {
        //            pendingOverVC.dogObjectDetails = [listData]
        //            pendingOverVC.catChosen = self.catChosen
        //
        //
        //        }
        //        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.groupTableViewBackground
        return headerView
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
        
        self.present(vc, animated: true, completion: nil)
        

        
        //        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        //        let controllerNav = UINavigationController(rootViewController: controller)
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //
        //        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        
        if startLocation == nil{
            //startLocation = latestLocation as CLLocation
            latestLocation = locations.last
            startLocation = latestLocation
            locationManager.stopUpdatingLocation()
            
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        
        let alertController = UIAlertController(title: "Location Accees Requested",
                                                message: "The location permission was not authorized. Please enable it in Settings to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            
            // THIS IS WHERE THE MAGIC HAPPENS!!!!
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings as URL)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
       
        print("Search word = \(searchBar.text?.uppercased())")
        
        
    }
    
//    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
//        print("hit")
//        return searchActive;
//    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchActive = true;
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("hit")
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
         //searchBar.text = ""
        //searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        searchActive = true
        self.tableView.reloadData()
        
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchText: searchText)
    }
    
    @IBAction func adImageButtonClose(_ sender: Any) {
        adImage.isHidden = true
        adImageViewHolder.isHidden = true
    }
    
    func handleTapGestures(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleAdTap(_sender:)))
        adImage.addGestureRecognizer(tap)
        adImage.isUserInteractionEnabled = true
        view.addSubview(adImage)
        
    }
    
    func handleAdTap(_sender: UITapGestureRecognizer){
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "AdDetailView") as! AdDetailView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        controller.dogObjects = self.dogObjects
        controller.catChosen = self.catChosen
        controller.counter = self.counter
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func search(searchText: String? = nil){
        print(searchText)
       
        let query = PFQuery(className: "All_Categories")
        //let query = PFQuery.orQuery(withSubqueries: [uppercase])
        print(categoryName)
        if categoryName == .none{
            print("none")
        } else {
//            query.whereKey("categories", matchesRegex: "(?i)\(search)")
            //lowercase.whereKey("categories", contains: categoryName)
            query.whereKey("categories", contains: categoryName)
            //allcase.whereKey("categories", contains: categoryName)
        }
        //        query.whereKey("categories", contains: categoryName)
        if(searchText != nil){
            
           
            //allcase.whereKey("name", matchesRegex: "(?i)\(searchText)")
            //lowercase.whereKey("name", contains: searchText?.lowercased())
            query.whereKey("name", contains: searchText?.uppercased())
           // allcase.whereKey("name", contains: searchText)
        }
        query.findObjectsInBackground { (results, error) -> Void in
            
            self.dogObjects = results!
            
            DispatchQueue.main.async {
                
                self.tableView.reloadData()
            }
            
        }
        
    }
    
    
    func categoryQuery(){
        
        //        let result = String(categoryName.characters.filter { ",".characters.contains($0)})
        
        let query:PFQuery = PFQuery(className: "All_Categories")
        query.whereKey("categories", contains: categoryName)
        query.findObjectsInBackground { (objects, error) in
            if objects != nil {
                self.dogObjects = objects!
                print(self.dogObjects)
                if self.dogObjects == []{
                    if self.categoryName == "Events"{
                        let alertController = UIAlertController(title: "Alert", message: "No Events", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                            
                            //                            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            //                            let vc: UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabBarController") as! UITabBarController
                            //
                            //                            self.present(vc, animated: true, completion: nil)
                            
                            
                            let controller = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let controllerNav = UINavigationController(rootViewController: controller)
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = controllerNav
                            
                            
                        })
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        print("no data")
                    }
                } else {
                    
                }
                //                self.tableView.reloadData()
                
            }
            
        }
        
    }
    
    func latestLocale(){
        if latestLocation == .none{
            self.locationManager.startUpdatingLocation()
        } else {
            
            let theLat = latestLocation.coordinate.latitude
            let theLng = latestLocation.coordinate.longitude
            let location = CLLocation(latitude: theLat, longitude: theLng)
            
            let location2 = CLLocation(latitude: latDbl, longitude: lngDbl)
            
            
            
            let distance = location.distance(from: location2)
            if distance <= 1609{
                //under 1 mile
            } else {
                //over 1 mile
                distanceInMiles = distance / 1609.344
                doubleStr = String(format: "%.1f", ceil(distanceInMiles*100)/100)
                
                locationManager.stopUpdatingLocation()
            }
            
            
            
            if let distance = doubleStr{
                
                cell.distanceFromCompanyLbl.text = "\(distance)"
                
                
            } else {
                print("no distance")
            }
            
            
        }
        
    }
    
    func queryAllCategories(){
        let query = PFQuery(className: "All_Categories")
        query.findObjectsInBackground { (objects, error) in
            if objects != nil{
                self.dogObjects = objects!
                self.tableView.reloadData()
            } else {
                
            }
        }
    }
    
    func adImageCounter(){
        if counter == 0{
            handleTapGestures()
            counter += 1
            if isImageAdded == false{
                
                adImageViewHolder.isHidden = false
                adImage.isHidden = false
                isImageAdded = true
            }
            
        } else if counter == 5{
            handleTapGestures()
            adImageViewHolder.isHidden = false
            adImage.isHidden = false
            counter = 0
            isImageAdded = false
        } else {
            counter += 1
            adImage.isHidden = true
            adImageViewHolder.isHidden = true
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("stoppted")
        queryAllCategories()
    }
   
}
