//
//  DirectionsView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/29/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Parse
class DirectionsView: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var mapView: MKMapView!
    var destination: MKMapItem?
    var locationManager: CLLocationManager = CLLocationManager()
    var userLocation: CLLocation?
    var lat:Double!
    var lng:Double!
    var dogDetails = [PFObject]()
    var directionsDetails = [String]()
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var instructionSteps: UITextView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "celll")
//        
//        let location = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
//        destination = MKMapItem(placemark: MKPlacemark(coordinate: location))
//        
//        mapView.delegate = self
//        mapView.showsUserLocation = true
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.delegate = self
//        locationManager.requestLocation()
        
        goToMap()
    }

   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0]
        self.getDirections()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func getDirections() {
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destination!
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculate(completionHandler: {(response, error) in
            
            if error != nil {
                print("Error getting directions")
            } else {
                self.showRoute(response!)
            }
        })
    }
    
    func showRoute(_ response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            mapView.add(route.polyline,
                         level: MKOverlayLevel.aboveRoads)
            
            for step in route.steps {
                print(step.instructions)
                directionsDetails.append(step.instructions)
                
            }
            self.tableView.reloadData()
        }
        
        let region =
            MKCoordinateRegionMakeWithDistance(userLocation!.coordinate,
                                               0.050, 0.050)
        
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor
        overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 5.0
        return renderer
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        controller.dogObjectDetails = dogDetails
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return directionsDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = directionsDetails[indexPath.row]
        return cell
    }
    
    func goToMap(){
        
//        var lat1 : NSString = self.venueLat
//        var lng1 : NSString = self.venueLng
        
        let latitude:CLLocationDegrees =  lat
        let longitude:CLLocationDegrees =  lng
        
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        
        let placemark : MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:nil)
        
        let mapItem:MKMapItem = MKMapItem(placemark: placemark)
        
        mapItem.name = "Target location"
        
        let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey as NSCopying)
        
        let currentLocationMapItem:MKMapItem = MKMapItem.forCurrentLocation()
        
        MKMapItem.openMaps(with: [currentLocationMapItem, mapItem], launchOptions: launchOptions as? [String : Any])
        
    }
}
