//
//  ReviewsAndRatingsView.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/28/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit
import Parse
class ReviewsAndRatingsView: UIViewController, UITextFieldDelegate {

    @IBOutlet var ratingSelected: CosmosView!
    @IBOutlet var reviewText: UITextView!
    
    var ratingChosen:Double!
    var companyName:String!
    var catChosen = [PFObject]()
    var dogDetailObjects = [PFObject]()
    var ratingObject = [PFObject]()
    var userReview:String!
    var rating:String!
    var count:String!
    var isNotFirstRating:Bool! = false
    var totalRatings:PFObject!
    var ratingsSearch:PFQuery<PFObject>!
    var totalRating = 0
    var totalCount = 0
    var nameToBeRemoved:String!
    var newRatingTotal:Int!
    var currObjId:String!
    var categoryName:String!
    var userImg:PFFile!
    var currentUser:PFUser!
    var counter:Int!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //query company to see if rating exist
        print(companyName)
        seeIfCompanyExists(name: companyName)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isReachable(){
            print("connection available")
        } else {
            addWifi()
        }

        
      currentUser = PFUser.current()
        //print(currentUser.username!)
        
        navBar()
        queryCurrentUser()
                
        //show keyboard - for user to enter review
        reviewText.becomeFirstResponder()
        
        //rating that was selected on DetailDogDataView
        ratingSelected.rating = ratingChosen
    }
    
    
    @IBAction func postReviewButtonTapped(_ sender: Any) {
        addNewRating()
      
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailDogDataView") as! DetailDogDataView
        let controllerNav = UINavigationController(rootViewController: controller)
        controller.dogObjectDetails = dogDetailObjects
        controller.catChosen = self.catChosen
        controller.currObjId = self.currObjId
        //controller.counter = self.counter
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
    }

    
 
    func seeIfCompanyExists(name:String!){
        let query = PFQuery(className: "CompanyRating")
        query.whereKey("name", contains: name!)
        query.findObjectsInBackground { (success, error) in
            if success != nil{
               
                for obj in success!{
                    print(name)
                    let name = obj.object(forKey: "name")as? String
                    print(name!)
                  
                    if name == ""{
                        print("empty")
                    } else {
                       self.isNotFirstRating = true
                        return
                    }
                }
                
                
                
            } else {
                print(error?.localizedDescription as Any)
            }
        }
        
    }
    
    func removeCurrentRating(name:String!){
     
        let totalCompanyRating = PFQuery(className: "TotalCompanyRating")
        totalCompanyRating.whereKey("name", equalTo: name)
        totalCompanyRating.whereKey("name", equalTo: name)
        
        totalCompanyRating.findObjectsInBackground { (success, error) in
            
            if success != nil{
                for mySuccess in success!{
                    mySuccess.deleteInBackground()
                    print(mySuccess)
                }
                //start adding new rating
                self.addNewRating()
            } else {
                
            }
        }
    }
    
    func addNewRating(){
        
        print(self.userImg)
        let date = NSDate()
        let dateFormatter = DateFormatter()
        //dateFormatter.dateStyle = DateFormatter.Style.FullStyle
        //dateFormatter.dateStyle = .full
        dateFormatter.dateFormat = "MM.dd.yyyy"
        let convertedDate = dateFormatter.string(from: date as Date)
        print(convertedDate)
        
        let reviewPost = PFObject(className: "Reviews")
        reviewPost["date"] = convertedDate
        reviewPost["review"] = reviewText.text
        reviewPost["rating"] = String(ratingChosen)
        //pull profile image
        reviewPost["reviewerImg"] = self.userImg
        reviewPost["name"] = companyName
        print(currentUser.username!)
        reviewPost["reviewer"] = currentUser.username!
        
        reviewPost.saveInBackground { (success, error) in
            
            if success{
                print("success")
                self.reviewText.text = .none
                self.addNewCompanyRating()
            } else {
                print("no success")
            }
        }

    }
    
    func addNewCompanyRating(){
        totalRatings = PFObject(className: "CompanyRating")
        totalRatings["name"] = companyName
        totalRatings["rating"] = String(ratingChosen)
        totalRatings["count"] = "1"
        
        totalRatings.saveInBackground { (success, error) in
            if success{
                print("new rating saved")
                self.runCompanyQuery()
            } else {
                print(error?.localizedDescription as Any)
            }
        }

    }
    
    func runCompanyQuery(){
        ratingsSearch = PFQuery(className: "CompanyRating")
        ratingsSearch.whereKey("name", contains: companyName)
        ratingsSearch.findObjectsInBackground { (success, error) in
            
            if success != nil{
                self.ratingObject = success!
                
                for rated in self.ratingObject{
                    print(self.companyName)
                    self.rating = rated.object(forKey: "rating")as! String
                    self.count = rated.object(forKey: "count")as! String
                    self.nameToBeRemoved = rated.object(forKey: "name")as! String
                    
                    let userRating = Double(self.rating)
                    let userCount = Int(self.count)
                    print(userCount!)
                    print(userRating!)
                    print(self.totalRating)
                    self.totalRating = Int(userRating!) + self.totalRating
                    print(self.totalRating)
                    
                    self.totalCount = userCount! + self.totalCount
                    print(self.totalCount)
                    //set a bool
                    
                }
                
            } else {
                print(error?.localizedDescription as Any)
            }
            //self.totalCount = self.totalCount + 1
            print(self.totalCount)
//            self.totalRating = self.totalRating + Int(self.ratingChosen)
            print(self.totalRating)
            self.newRatingTotal = self.totalRating / self.totalCount
            
            
            print(self.catChosen)
            for category in self.catChosen{
                self.categoryName = category.object(forKey: "category_name")as? String
            }
            let currString = String(self.currObjId)
            print(currString!)
            let query = PFQuery(className: "All_Categories")
            query.whereKey("categories", contains: self.categoryName)
            query.getObjectInBackground(withId: currString!, block: { (ratingUpdated:PFObject?, error) in
                if error != nil{
                   print(error as Any)
                } else if let newRatingUpdated = ratingUpdated {
                   print(self.newRatingTotal)
                    newRatingUpdated["rating"] = String(self.newRatingTotal)
                    newRatingUpdated.saveInBackground(block: { (success, error) in
                        if success{
                            print("we have sucess")
                            self.ratingSelected.rating = 0
                            //move back to category section
                            let controller = self.storyboard!.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
                            let controllerNav = UINavigationController(rootViewController: controller)
//                            controller.dogObjectDetails = dogDetailObjects
                            controller.catChosen = self.catChosen
                            controller.dogObjects = self.dogDetailObjects
//                            controller.counter = self.counter
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = controllerNav
                        } else{
                            print(error?.localizedDescription as Any)
                        }
                    })
                }
            })
        }

    }
    
    func queryCurrentUser(){
    
        let currentUser = PFUser.current()?.username
        
        let query = PFQuery(className: "_User")
        query.whereKey("username", contains: currentUser)
        query.whereKey("username", equalTo: currentUser!)
        query.whereKeyExists("profile_picture")
        query.findObjectsInBackground { (objects, error) in
            print(objects!)
            if objects != nil{
                for object in objects!{
                    self.userImg = object.object(forKey: "profile_picture")as? PFFile
                }
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        reviewText.resignFirstResponder()
        addNewRating()
        return true
    }
}
