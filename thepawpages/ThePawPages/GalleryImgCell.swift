//
//  GalleryImgCell.swift
//  ThePawPages
//
//  Created by Steven Roseman on 3/28/17.
//  Copyright © 2017 Steven Roseman. All rights reserved.
//

import UIKit

class GalleryImgCell: UICollectionViewCell {
  
    @IBOutlet var galleryImg: UIImageView!
   
    
}
